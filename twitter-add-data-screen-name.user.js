// ==UserScript==
// @name         twitter-add-data-screen-name
// @namespace    https://gitlab.com/mpsuzuki/www-user-scripts/
// @version      0.3.0
// @description  add data-screen-name
// @author       mpsuzuki@hiroshima-u.ac.jp
// @include      https://twitter.com/*
// @run-at       document-end
// @grant        none
// @require      https://greasemonkey.github.io/gm4-polyfill/gm4-polyfill.js
// ==/UserScript==

const scriptName = "twitter-add-data-screen-name";

const divSelector = [
    "div[data-testid='tweet']",
    "div[data-testid='UserCell']",
    "div[data-testid='trend']",
    "div[role='blockquote']",
    "a[href^='/i/events/']"
].join(", ");

var moCallBack = function(records) {
    console.log(scriptName + ": moCallBack is kicked by " + records.length + " mutations");

    var addDataScreenName = function(e) {
        var d = e;
        var testID = e.getAttribute("data-testid");
        var role = e.getAttribute("role");
        var href = e.getAttribute("href");
        var typ = "unknown";
        switch (testID)
        {
            case "tweet": typ = "tweet"; break;
            case "UserCell": typ = "recommended-user"; break;
            case "trend": typ = "trend"; break;
            default:
                switch (role)
                {
                    case "blockquote": typ = "quoted-tweet"; break;
                    case "link": typ = "promotion"; d = e.parentElement;
                };
        };

        if (typ == "promotion" || typ == "trend") {
            console.log(scriptName + ": no account for " + typ + ":" + d.textContent);
            d.setAttribute("data-plaintext", d.textContent);
        };

        { // SVG linked marks
            let svgs = d.getElementsByTagName("svg");
            for (let i = 0; i < svgs.length; i ++) {
                let svg = svgs[i];
                let pSvg = svg.parentElement;
                let ppSvg = pSvg.parentElement;
                let strDomain = ppSvg.textContent.trim();
                if (pSvg.tagName == "SPAN" &&
                    ppSvg.tagName == "DIV" &&
                    svg.getAttribute("viewBox").trim().split(/\s+/).join(" ") == "0 0 24 24") {
                    console.log(scriptName + ": linked domain " + typ + ":" + strDomain);
                    d.setAttribute("data-domain", strDomain);
                    let imgs = d.getElementsByTagName("IMG");
                    for (let j = 0; j < imgs.length; j ++) {
                        imgs[j].parentElement.setAttribute("data-img-domain", strDomain);
                    };
                };
            };
        };

        var spans = d.getElementsByTagName("span");
        for (var j = 0; j < spans.length; j++) {
            var t = spans[j].textContent;
            if (t.length < 17 && /^@[0-9A-Za-z_]+$/.test(t)) {
                d.setAttribute("data-screen-name", t.slice(1));
                console.log(scriptName + ": add data-screen-name " + t + " to " + typ + ":" + d.textContent);
                break;
            };
        };
    };

    records.forEach(function(r){
        switch (r.type) {
        case "childList":
            r.addedNodes.forEach(function(n){
                var divs = n.querySelectorAll(divSelector);
                for (var i = 0; i < divs.length; i++) {
                    addDataScreenName(divs[i]);
                };
            });
            break;
        case "characterData":
            let e = r.target;
            while (e != null) {
                e = e.parentElement;
                if (e.getAttribute("role") != null) {
                    console.log("  found role attribute for ascendants of " + r.target.trim());
                    break;
                } else
                if (e.getAttribute("data-test-id") != null) {
                    console.log("  found data-test-id attribute for ascendants of " + r.target.trim());
                    break;
                };
            };
            if (e != null) {
                addDataScreenName(e);
            };
        };
    });
};

const moOption = {
    // attributes: true,
    // characterData: true,
    childList: true,
    subtree: true
};
const mo = new MutationObserver(moCallBack);
mo.observe(document.body, moOption);
console.log(scriptName + ": start observation");

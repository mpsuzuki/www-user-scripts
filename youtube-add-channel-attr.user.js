// ==UserScript==
// @name         youtube-add-channel-attr
// @namespace    https://gitlab.com/mpsuzuki/www-user-scripts/
// @version      0.0.99
// @description  add channel name to data-channel-name attribute
// @author       mpsuzuki@hiroshima-u.ac.jp
// @include      https://www.youtube.com/*
// @include      https://youtube.com/*
// @run-at       document-end
// @grant        none
// @require      https://greasemonkey.github.io/gm4-polyfill/gm4-polyfill.js
// ==/UserScript==

const divSelector = [
    "yt-formatted-string.ytd-channel-name"
].join(", ");

const moCallBack = function(records) {
    console.log("add_channel_attr: moCallBack is kicked by " + records.length + " mutations");

    const getYTdCompactVideoRenderer = function(e) {
        while (e != null) {
            // console.log("    " + e.tagName);
            if (e.tagName == "YTD-COMPACT-VIDEO-RENDERER") {
                return e;
            };
            e = e.parentElement;
        };
        return e;
    };

    const getVideoTitle = function(e) {
        const spans = e.getElementsByTagName("span");
        for (let i = 0; i < spans.length; i ++) {
            if (spans[i].id == "video-title") {
                return spans[i].textContent.trim();
            }
        };
    };

    const addYTdChannelAttr = function(eYtFmtStrings_ClsChannelName) {
        const channelName = eYtFmtStrings_ClsChannelName.textContent;
        console.log("test for \"" + channelName + "\"");
        const e = getYTdCompactVideoRenderer(eYtFmtStrings_ClsChannelName);
        if (!e) {
            console.log("cannot find a parental ytd-compact-video-renderer element for " + eYtFmtStrings_ClsChannelName);
            return;
        };
        console.log("  add data-channel-name attribute(" + channelName + ")");
        e.setAttribute("data-channel-name", channelName);
        const videoTitle = getVideoTitle(e);
        console.log("  add data-video-title attribute(" + videoTitle + ")");
        e.setAttribute("data-video-title", videoTitle);
        console.log("  ok");
        return;
    };

    records.forEach(function(r){
        switch (r.type) {
        case "childList":
            console.log("childList");
            r.addedNodes.forEach(function(n){
                const divs = n.querySelectorAll(divSelector);
                for (let i = 0; i < divs.length; i++) {
                    addYTdChannelAttr(divs[i]);
                };
            });
            break;
        case "characterData":
            var e = r.target.parentElement;
            console.log("characterData: " + e.tagName + "->" + r.target.textContent.trim());
            if ( e.tagName == "SPAN" && e.classList.contains("ytd-channel-name")) {
                addYTdChannelAttr( e );
            };
            break;
        };
    });
};

const moOption = {
    characterData: true,
    childList: true,
    subtree: true
};
const mo = new MutationObserver(moCallBack);
mo.observe(document.body, moOption);
console.log("youtube-add-channel-attr: start observation");
